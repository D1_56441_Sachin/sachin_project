package com.app.airline.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.airline.dtos.Response;
import com.app.airline.pojos.Passenger;
import com.app.airline.services.PassengerServiceImpl;

@RestController
@CrossOrigin("*")
@RequestMapping("/user")
public class PassengerController {
	
	@Autowired
	private PassengerServiceImpl pService;
	
	@PostMapping("/addPassenger")
	public ResponseEntity<?> addPassenger(@RequestBody Passenger passenger) {
		Passenger passenger2=pService.addPassenger(passenger);
		if(passenger!=null)
		return Response.success(passenger2);
		else
			return Response.error("Error while adding passenger");
	}
	
	@GetMapping("/getPassengerById/{id}")
	public ResponseEntity<?> getPassengerById(@PathVariable("id") int id){
		Passenger passenger=pService.findById(id);
		if(passenger!=null)
			return Response.success(passenger);
			else
				return Response.error("No passenger found");
	}
	@PutMapping("/addPassenger")
	public ResponseEntity<?> addOrUpdatePassenger(@RequestBody Passenger passenger) {
		Passenger passenger2 = pService.saveOrUpdate(passenger);
		if(passenger2!=null)
			return Response.success(passenger2);
			else
				return Response.error("Error while adding passenger");
	}
	
	// Find all Passengers
		@GetMapping("/")
		public ResponseEntity<?> Findall() {
			try {
				List<Passenger> result = pService.findAll();
				if (result == null)
					return Response.error("No Passengers exists");
				return Response.success(result);
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
		}
		// Update passenger
		@PutMapping("/{id}")
		public ResponseEntity<?> updatePassenger(@PathVariable("id") int id, @Valid @RequestBody Passenger passenger){
			try {
				Passenger p=pService.findPassengerById(id);
				if(p==null)
					return Response.error("Passenger by this id does not exists");
				else
					{
					p.setTitle(passenger.getTitle());
					p.setAddress(passenger.getAddress());
					p.setCovidVaccinated(passenger.getCovidVaccinated());
					p.setEmail(passenger.getEmail());
					p.setMobileNo(passenger.getMobileNo());
					Passenger p1=pService.updatePassenger(p);
					return Response.success(p1);
					}
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
		}
		
	// Delete Passenger by Id
		@DeleteMapping("/{id}")
		public ResponseEntity<?> deletePassenger(@PathVariable("id") int id) {
			try {
				Passenger result = pService.findPassengerById(id);
				if (result == null)
					return Response.error("Passenger by this id does not exists");
				else
				{
					int x=pService.deletePassenger(id);
					return Response.success(x+" Passenger deleted");
				}
				
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
		}

}
