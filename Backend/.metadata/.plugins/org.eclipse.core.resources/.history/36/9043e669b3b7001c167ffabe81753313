package com.app.airline.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.airline.dao.IBookingDetailsProjection;
import com.app.airline.dtos.BookingDto;
import com.app.airline.dtos.Response;
import com.app.airline.pojos.Booking;
import com.app.airline.services.BookingServiceImpl;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/user/booking")
public class BookingController {
	
	@Autowired
	private BookingServiceImpl bService;
	
	
	@GetMapping("/")
	public ResponseEntity<?> searchBooking() {
		
		List<Object []> result = bService.findBooking();
		if (result == null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(result); 
	}
	
	@GetMapping("/all")
	public ResponseEntity<?> searchAllBooking() {
		
		List<Booking> result = bService.findAllBookings();
		if (result == null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(result); 
	}
	
	@GetMapping("/{bookingId}/{firstName}")
	public ResponseEntity<?> searchBooking(@PathVariable(value = "bookingId") int id, @PathVariable(value = "firstName") String name) {
		
		List<Object []> result = bService.searchBooking(id, name);
		if (result == null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(result); 
	}
	
	@PostMapping("/addBooking")
	public Booking addBooking(@RequestBody BookingDto bookingDto) {
		Booking b=bService.addBooking(bookingDto);
		if(b==null)
			return null;
		return b;
		
	}
	
	@GetMapping("/getAll")
	public List<IBookingDetailsProjection> getAll(){
		return bService.getAllBookingDetails();
	}
	
	@GetMapping("/getBookingDetailsById/{id}")
	public IBookingDetailsProjection getBookingDetailsById(@PathVariable("id") int id){
		return bService.getAllBookingDetailsById(id);
	}
	
//	@GetMapping("/getSeatNo/{id}")
//	public int getSeatNo(@PathVariable("id") int scheduleId){
//		return bService.getSeatNo(scheduleId);
//		
//	}
	
	@GetMapping("/getSeatNo/{id}")
	public List<Integer> getSeatNo(@PathVariable("id") int scheduleId){
		return bService.getSeatNo(scheduleId);
		
	}
	
	@PutMapping("/checkIn/{bookingId}/{firstName}")
	public ResponseEntity<?> checkIn(@PathVariable("bookingId") int bookingId, @PathVariable("firstName") String firstName){
		
		Object result= bService.checkIn(bookingId, firstName);
		
		if(result==null)
			return Response.error("Booking with given details not found");
		else if(result=="Confirmed")
			return Response.error("Already Checked In");
		else if(result=="Cancelled")
			return Response.error("This Ticket is Cancelled");
		return Response.success(result);
		
	}
	
	@GetMapping("/myTrip/{bookingId}/{firstName}")
	public ResponseEntity<?> myTrip(@PathVariable("bookingId") int bookingId, @PathVariable("firstName") String firstName){
		
		Booking result= bService.myTrip(bookingId, firstName);
		
		if(result==null)
			return Response.error("Booking with given details not found");
		return Response.success(result);
		
	}
	
	@PutMapping("/cancelTicket/{bookingId}")
	public ResponseEntity<?> cancelTicket(@PathVariable("bookingId") int bookingId){
		
		Object result= bService.cancelTicket(bookingId);
		
		if(result==null)
			return Response.error("Booking with given details not found");
		else if(result=="Cancelled")
			return Response.error("Booking Already Cancelled");
		return Response.success(result);
		
	}

}
