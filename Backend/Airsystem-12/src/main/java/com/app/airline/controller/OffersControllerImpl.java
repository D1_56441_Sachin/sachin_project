package com.app.airline.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.airline.dtos.Response;
import com.app.airline.pojos.Offers;
import com.app.airline.services.OffersServiceImpl;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/offers")
public class OffersControllerImpl {

	@Autowired
	private OffersServiceImpl offerService;

// Get Offers by ID
	@GetMapping("/{id}")
	public ResponseEntity<?> FindOffersById( @PathVariable("id") int id) {
		try {
			Offers result = offerService.findOfferById(id);
			if (result == null)
				return Response.error("Offers by this id does not exists");
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Find all Offerss
	@GetMapping("/")
	public ResponseEntity<?> Findall() {
		try {
			List<Offers> result = offerService.findAllOfferss();
			if (result == null)
				return Response.error("No Offerss exists");
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
// Add Offers
	@PostMapping("/add")
	public ResponseEntity<?> addOffers (@Valid @RequestBody Offers Offers)
	{
		try {
			int count= offerService.addOffer(Offers);
			if(count==0)
				return Response.error("Failed to add Offers");
		return Response.success(count+" Offer added");	
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
// Update Offers
	@PutMapping("/{id}")
	public ResponseEntity<?> updateOffers(@PathVariable("id") int id, @Valid @RequestBody Offers Offers){
		try {
			Offers p=offerService.findOfferById(id);
			if(p==null)
				return Response.error("Offers by this id does not exists");
			else
				{
				p.setDiscount(Offers.getDiscount());
				p.setMinTxnAmount(Offers.getMinTxnAmount());
				//p.setValid_on(Offers.getValid_on());
				//p.setPromocode(Offers.getPromocode());
				Offers p1=offerService.updateOffer(p);
				return Response.success(p1);
				}
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
// Delete Offers by Id
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteOffers(@PathVariable("id") int id) {
		try {
			Offers result = offerService.findOfferById(id);
			if (result == null)
				return Response.error("Offers by this id does not exists");
			else
			{
				int x=offerService.deleteOffersById(id);
				return Response.success(x+" Offers deleted");
			}
			
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
}
