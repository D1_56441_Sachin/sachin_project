package com.app.airline.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.airline.dao.IPassenger;
import com.app.airline.pojos.Passenger;


@Service
@Transactional
public class PassengerServiceImpl {
	
	@Autowired
	private IPassenger pDao;
	
	public Passenger findById(int id) {
		Passenger p=pDao.findById(id);
		return p;
	}
	
	public Passenger addPassenger(Passenger passenger) {
		Passenger p=pDao.save(passenger);
		return p;
	}

	public Passenger findPassenger(String firstName, String lastName, String email) {
		return pDao.findByFirstNameAndLastNameAndEmail(firstName, lastName, email);
	}
	
	public Passenger saveOrUpdate(Passenger p) {
		Passenger p2=pDao.findByFirstNameAndLastNameAndEmail(p.getFirstName(), p.getLastName(), p.getEmail());
		
		if(p2==null) {
			return pDao.save(p);
		}else {
			p2.setTitle(p.getTitle());
			p2.setMobileNo(p.getMobileNo());
			p2.setDateOfBirth(p.getDateOfBirth());
			p2.setCovidVaccinated(p.getCovidVaccinated());
			p2.setAddress(p.getAddress());
			p2.setCity(p.getCity());
			p2.setPassState(p.getPassState());
			p2.setCountry(p.getCountry());
			
			
			return pDao.save(p2);
		}
	}
	
	//find all passengers
		public List<Passenger> findAll(){
			return pDao.findAll();
		}
		
		// Update passenger
		public Passenger updatePassenger(Passenger passenger) {
			
			return pDao.save(passenger);
		}
		
		//Delete Passenger By id
		public int deletePassenger(int id) {
			if(pDao.existsById(id))
			{
				pDao.deleteById(id);
			return 1;
			}else
				return 0;
		}
		
		//Find passenger by id
		public Passenger findPassengerById(int id)
		{
			return pDao.findById(id);
		}
}
