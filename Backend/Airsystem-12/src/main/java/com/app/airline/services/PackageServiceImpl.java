package com.app.airline.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.airline.dao.IPackagesDao;
import com.app.airline.pojos.Packages;


@Service
@Transactional
public class PackageServiceImpl {

	@Autowired
	private IPackagesDao packagesDao;
	
	public List<Packages> getAllPackages(){
		return packagesDao.findAll();
		
	}
	
	// Find by id
		public Packages findPackageById(int packageId) {
			Optional<Packages> package1 = packagesDao.findById(packageId);
			return package1.orElse(null);
		}
		
		// Add package
		public Packages addPackage(Packages Package) {
			return packagesDao.save(Package);
		}
		
		// Update package
		public Packages updatePackage(Packages Package) {

			return packagesDao.save(Package);
		}
		
		// Delete package
		public int deletePackageById(int id) {

			if (packagesDao.existsById(id)) {
				packagesDao.deleteById(id);
				return 1;
			} else
				return 0;
		}
}
