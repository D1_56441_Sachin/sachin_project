package com.app.airline.pojos;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "payment_card_details")
public class PaymentCardDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "card_id")
	private int id;
//	@ManyToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "user_id")
//	private User user;
	@Column(name = "user_id")
	private int userId;
	@Column(name = "name_on_card")
	private String nameOnCard;
	@Column(name = "card_no")
	private long cardNo;
	@Column(name = "valid_till")
	private Date validityDate;
	private int cvv;
	
	public PaymentCardDetails() {
	}

	public PaymentCardDetails(int id, String nameOnCard, long cardNo, Date validityDate, int cvv) {
		super();
		this.id = id;
		this.nameOnCard = nameOnCard;
		this.cardNo = cardNo;
		this.validityDate = validityDate;
		this.cvv = cvv;
	}

	public PaymentCardDetails(String nameOnCard, long cardNo, Date validityDate, int cvv) {
		super();
		this.nameOnCard = nameOnCard;
		this.cardNo = cardNo;
		this.validityDate = validityDate;
		this.cvv = cvv;
	}
	
	public PaymentCardDetails(int id, int user_id, String nameOnCard, long cardNo, Date validityDate, int cvv) {
		super();
		this.id = id;
		this.userId = user_id;
		this.nameOnCard = nameOnCard;
		this.cardNo = cardNo;
		this.validityDate = validityDate;
		this.cvv = cvv;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public long getCardNo() {
		return cardNo;
	}

	public void setCardNo(long cardNo) {
		this.cardNo = cardNo;
	}

	public Date getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(Date validityDate) {
		this.validityDate = validityDate;
	}

	public int getCvv() {
		return cvv;
	}

	public void setCvv(int cvv) {
		this.cvv = cvv;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int user_id) {
		this.userId = user_id;
	}

	@Override
	public String toString() {
		return "PaymentCardDetails [id=" + id + ", user_id=" + userId + ", nameOnCard=" + nameOnCard + ", cardNo="
				+ cardNo + ", validityDate=" + validityDate + ", cvv=" + cvv + "]";
	}

	

	


	
	
}
