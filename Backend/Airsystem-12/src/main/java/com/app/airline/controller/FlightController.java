package com.app.airline.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.airline.dtos.FlightDto;
import com.app.airline.dtos.FlightScheduleDto;
import com.app.airline.dtos.FlightScheduleDtoProjection;
import com.app.airline.dtos.Response;
import com.app.airline.pojos.Flight;
import com.app.airline.services.FlightServiceImpl;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/admin/flight")
public class FlightController {

	@Autowired
	private FlightServiceImpl fService;

	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") int id) {

		Flight result = fService.findFlightById(id);
		System.out.println(result.getScheduleList());
		if (result == null)
			return Response.error("No flight found");
		return Response.success(result); // http status = 200, body = result

	}

	// Add flight
	@PostMapping("/add")
	public ResponseEntity<?> addFlight(@RequestBody Flight flight) {
		try {
			Flight f = fService.addFlight(flight);
			if (f == null)
				return Response.error("Failed to add flight");
			return Response.success(f);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

	@PostMapping("/addFlightAndSchedule")
	public ResponseEntity<?> addFlightAndSchudle(@RequestBody FlightDto flightDto) {
		System.out.println("Inserting: " + flightDto);
		Map<String, Object> result = fService.addFlightAndSchedule(flightDto);
		return ResponseEntity.ok(result);
	}

//	@GetMapping("/flightschudles")
//	public ResponseEntity<?> findAll() {
//
//			List<FlightScheduleDto> result = fService.getFlightDetails();
//			if (result == null)
//				return ResponseEntity.notFound().build();
//			return ResponseEntity.ok(result); // http status = 200, body = result
//		
//	}
//
//	@GetMapping("/searchFlights")
//	public ResponseEntity<?> searchFlights(@RequestBody FlightDto dto) {
//			
//			List<FlightScheduleDto> result = fService.searchFlights(dto);
//			if (result == null)
//				return ResponseEntity.notFound().build();
//			return ResponseEntity.ok(result); // http status = 200, body = result
//		
//	}

	@GetMapping("/searchFlights1/{source}/{destination}/{departureDate}")
	public ResponseEntity<?> searchFlights1(@PathVariable(value = "source") String sourceCity,
			@PathVariable(value = "destination") String destinationCity,
			@PathVariable(value = "departureDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date departureDate) {
		System.out.println(departureDate);
		List<FlightScheduleDto> result = fService.searchFlights1(destinationCity, sourceCity, departureDate);
		if (result.isEmpty())
			//return ResponseEntity.notFound().build();
			return Response.error("Flight Not Available On This Journye");
		return Response.success(result); // http status = 200, body = result

	}

	// Find all
	@GetMapping("/")
	public ResponseEntity<?> findAll() {
		try {
			List<Flight> flights = fService.findAllFlight();
			if (flights != null)
				return Response.success(flights);
			else
				return Response.error("No flight details available right now !!!");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

	// Update Flight
	@PutMapping("/{id}")
	public ResponseEntity<?> updateFlight(@PathVariable("id") int id, @RequestBody Flight flight) {
		try {
			Flight f = fService.findFlightById(id);
			if (f == null)
				return Response.error("Flight by this id does not exists");
			else {
				// f.setAirline_name(flight.getAirline_name());
				// f.setBusiness_class_capacity(flight.getBusiness_class_capacity());
				f.setBusinessClassFare(flight.getBusinessClassFare());
				// f.setEconomy_class_capacity(flight.getEconomy_class_capacity());
				f.setEconomyClassFare(flight.getEconomyClassFare());
				fService.updateFlight(f);
				return Response.success(f);
			}
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

	// Delete Flight by Id
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteFlight(@PathVariable("id") int id) {
		try {
			Flight result = fService.findFlightById(id);
			if (result == null)
				return Response.error("Flight by this id does not exists");
			else {
				int x = fService.deletePassenger(id);
				return Response.success(x + " Flight deleted");
			}

		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

	// Check Flight statust
	@GetMapping("/checkStatus/{flightid}/{date}")
	public ResponseEntity<?> searchFlight(@PathVariable(value = "flightid") int flightid,
			@PathVariable(value = "date")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date departureDate) {
		try {
			System.out.println(flightid + " " + departureDate);
			List<FlightScheduleDtoProjection> list = fService.checkFlightStatus(flightid, departureDate);
			if (list == null)
				return Response.error("Flight Not available for this journey");
			else
				return Response.success(list);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

}
