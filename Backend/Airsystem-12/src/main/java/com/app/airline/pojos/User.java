package com.app.airline.pojos;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "user")
public class User {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "user_id")
	private int id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Email
	@NotBlank
	private String email;

	private String password;
	private String role;
	
//	@OneToMany(mappedBy = "user")
//	private List<PaymentCardDetails> listOfCards;
	
	@OneToMany(mappedBy = "userId")
	private List<PaymentCardDetails> listOfCards;
	
	@OneToMany(mappedBy = "user")
	private List<Booking> list;

	public User() {

	}

	public User(int id) {

		this.id = id;
	}

	public User(int id, String first_name, String last_name, String email, String password, String role) {

		this.id = id;
		this.firstName = first_name;
		this.lastName = last_name;
		this.email = email;
		this.password = password;
		this.role = role;
	}

	public User(String firstName, String lastName, @Email @NotBlank String email, String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String first_name) {
		this.firstName = first_name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String last_name) {
		this.lastName = last_name;
	}

	public void setListOfCards(List<PaymentCardDetails> listOfCards) {
		this.listOfCards = listOfCards;
	}

	public void setList(List<Booking> list) {
		this.list = list;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", first_name=" + firstName + ", last_name=" + lastName + ", email=" + email
				+ ", password=" + password + ", role=" + role + "]";
	}

}
