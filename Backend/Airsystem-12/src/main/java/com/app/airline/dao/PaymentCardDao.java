package com.app.airline.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.airline.pojos.PaymentCardDetails;

public interface PaymentCardDao extends JpaRepository<PaymentCardDetails, Integer> {

	PaymentCardDetails findById(int id);

	List<PaymentCardDetails> findByUserId(int userId);
}
