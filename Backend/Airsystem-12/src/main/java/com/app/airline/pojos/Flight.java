package com.app.airline.pojos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "flight")
public class Flight {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "flight_id")
	private int flightId;

	@Column(name = "airline_name")
	private String airlineName;

	@Column(name = "departure_airport_name")
	private String departureAirportName;

	@Column(name = "arrival_airport_name")
	private String arrivalAirportName;

	@Column(name = "source_city")
	private String sourceCity;

	@Column(name = "destination_city")
	private String destinationCity;

	@Column(name = "business_class_fare")
	private double businessClassFare;

	@Column(name = "economy_class_fare")
	private double economyClassFare;

	@Column(name = "business_class_capacity")
	private int businessClassCapacity;

	@Column(name = "economy_class_capacity")
	private int economyClassCapacity;

	@Column(name = "flight_total_capacity")
	private int flightTotalCapacity;
	@OneToMany(mappedBy = "flight",cascade = CascadeType.REMOVE)
	@JsonIgnoreProperties("flight")
//	@OneToMany(targetEntity = Schedule.class,cascade = CascadeType.ALL)
//	@JoinColumn(name = "flight_id", referencedColumnName = "flight_id")
	private List<Schedule> scheduleList;
	
	@OneToMany(mappedBy = "flight")
	private List<Booking> bookingList;

	public Flight() {
		this.scheduleList=new ArrayList<Schedule>();
	}

	public Flight(int flightId, String airlineName, String departureAirportName, String arrivalAirportName, String sourceCity,
			String destinationCity, double businessClassFare, double economyClassFare, int businessClassCapacity,
			int economyClassCapacity, int flightTotalCapacity) {
		super();
		this.flightId = flightId;
		this.airlineName = airlineName;
		this.departureAirportName = departureAirportName;
		this.arrivalAirportName = arrivalAirportName;
		this.sourceCity = sourceCity;
		this.destinationCity = destinationCity;
		this.businessClassFare = businessClassFare;
		this.economyClassFare = economyClassFare;
		this.businessClassCapacity = businessClassCapacity;
		this.economyClassCapacity = economyClassCapacity;
		this.flightTotalCapacity = flightTotalCapacity;
		this.scheduleList=new ArrayList<Schedule>();
	}

	public Flight(String airlineName, String departureAirportName, String arrivalAirportName, String sourceCity,
			String destinationCity, double businessClassFare, double economyClassFare, int businessClassCapacity,
			int economyClassCapacity, int flightTotalCapacity) {
		super();
		this.airlineName = airlineName;
		this.departureAirportName = departureAirportName;
		this.arrivalAirportName = arrivalAirportName;
		this.sourceCity = sourceCity;
		this.destinationCity = destinationCity;
		this.businessClassFare = businessClassFare;
		this.economyClassFare = economyClassFare;
		this.businessClassCapacity = businessClassCapacity;
		this.economyClassCapacity = economyClassCapacity;
		this.flightTotalCapacity = flightTotalCapacity;
		this.scheduleList=new ArrayList<Schedule>();
	}

	public int getFlightId() {
		return flightId;
	}

	public List<Schedule> getScheduleList() {
		return scheduleList;
	}

	public void setScheduleList(List<Schedule> scheduleList) {
		this.scheduleList = scheduleList;
	}

	public void setFlightId(int id) {
		this.flightId = id;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public String getDepartureAirportName() {
		return departureAirportName;
	}

	public void setDepartureAirportName(String departureAirportName) {
		this.departureAirportName = departureAirportName;
	}

	public String getArrivalAirportName() {
		return arrivalAirportName;
	}

	public void setList(List<Booking> list) {
		this.bookingList = list;
	}

	public void setArrivalAirportName(String arrivalAirportName) {
		this.arrivalAirportName = arrivalAirportName;
	}

	public String getSourceCity() {
		return sourceCity;
	}

	public void setSourceCity(String sourceCity) {
		this.sourceCity = sourceCity;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public double getBusinessClassFare() {
		return businessClassFare;
	}

	public void setBusinessClassFare(double businessClassFare) {
		this.businessClassFare = businessClassFare;
	}

	public double getEconomyClassFare() {
		return economyClassFare;
	}

	public void setEconomyClassFare(double economyClassFare) {
		this.economyClassFare = economyClassFare;
	}

	public int getBusinessClassCapacity() {
		return businessClassCapacity;
	}

	public void setBusinessClassCapacity(int businessClassCapacity) {
		this.businessClassCapacity = businessClassCapacity;
	}

	public int getEconomyClassCapacity() {
		return economyClassCapacity;
	}

	public void setEconomyClassCapacity(int economyClassCapacity) {
		this.economyClassCapacity = economyClassCapacity;
	}

	public int getFlightTotalCapacity() {
		return flightTotalCapacity;
	}

	public void setFlightTotalCapacity(int flightTotalCapacity) {
		this.flightTotalCapacity = flightTotalCapacity;
	}

	@Override
	public String toString() {
		return "Flight [flightId=" + flightId + ", airlineName=" + airlineName + ", departureAirportName=" + departureAirportName
				+ ", arrivalAirportName=" + arrivalAirportName + ", sourceCity=" + sourceCity + ", destinationCity="
				+ destinationCity + ", businessClassFare=" + businessClassFare + ", economyClassFare="
				+ economyClassFare + ", businessClassCapacity=" + businessClassCapacity + ", economyClassCapacity="
				+ economyClassCapacity + ", flightTotalCapacity=" + flightTotalCapacity + "]";
	}

}
