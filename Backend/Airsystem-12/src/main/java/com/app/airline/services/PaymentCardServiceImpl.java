package com.app.airline.services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.airline.dao.PaymentCardDao;
import com.app.airline.dtos.Response;
import com.app.airline.pojos.PaymentCardDetails;

@Transactional
@Service
public class PaymentCardServiceImpl {

	@Autowired
	private PaymentCardDao paymentCardDao;

//Find all
	public List<PaymentCardDetails> findAllPaymentCardDetails() {
		return paymentCardDao.findAll();
	}

//Add PaymentCardDetails
	public PaymentCardDetails addPaymentCardDetails(PaymentCardDetails paymentCardDetails) {
		return paymentCardDao.save(paymentCardDetails);
	}

//Find PaymentCardDetails by id
	public PaymentCardDetails findPaymentCardDetailsById(int id) {
		return paymentCardDao.findById(id);
	}

// Update PaymentCardDetails
	public PaymentCardDetails updatePaymentCardDetails(PaymentCardDetails paymentCardDetails) {

		PaymentCardDetails card = paymentCardDao.findById(paymentCardDetails.getId());
		if (card == null)
			return null;
		else {
			card.setCardNo(paymentCardDetails.getCardNo());
			card.setNameOnCard(paymentCardDetails.getNameOnCard());
			card.setValidityDate(paymentCardDetails.getValidityDate());
			card.setCvv(paymentCardDetails.getCvv());
			paymentCardDao.save(card);
			return card;
		}
	}

//Delete PaymentCardDetails By id
	public int deletePaymentCardDetails(int id) {
		if (paymentCardDao.existsById(id)) {
			paymentCardDao.deleteById(id);
			return 1;
		} else
			return 0;
	}

	public PaymentCardDetails addpaymentCardDetails(@Valid PaymentCardDetails paymentCardDetails) {
		return paymentCardDao.save(paymentCardDetails);
	}

	public List<PaymentCardDetails> findPaymentCardDetailsByUserId(int userId) {
		return paymentCardDao.findByUserId(userId);
	}
}
