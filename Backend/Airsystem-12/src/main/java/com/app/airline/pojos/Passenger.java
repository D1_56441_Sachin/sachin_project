package com.app.airline.pojos;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;

@Entity
@Table(name = "passenger")
public class Passenger {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "passenger_id")
	private int id;
	
	private String title;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "mobile_no")
	private long mobileNo;
	
	@Email
	private String email;
	private String country;
	@Column(name = "pass_state")
	private String passState;
	private String city;
	private String address;
	
	@Column(name = "date_of_birth")
	private Date dateOfBirth;
	
	@Column(name = "covid_vaccinated")
	private short covidVaccinated;
	
	@OneToMany(mappedBy = "passenger")
	private List<Booking> blist;

	public Passenger() {

	}

	public Passenger(String title, String firstName, String lastName, long mobileNo, @Email String email,
			String country, String passState, String city, String address, Date dateOfBirth, short covidVaccinated) {
		super();
		this.title = title;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNo = mobileNo;
		this.email = email;
		this.country = country;
		this.passState = passState;
		this.city = city;
		this.address = address;
		this.dateOfBirth = dateOfBirth;
		this.covidVaccinated = covidVaccinated;
	}

	public Passenger(int id, String title, String firstName, String lastName, long mobileNo, @Email String email,
			String country, String passState, String city, String address, Date dateOfBirth, short covidVaccinated) {
		super();
		this.id = id;
		this.title = title;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNo = mobileNo;
		this.email = email;
		this.country = country;
		this.passState = passState;
		this.city = city;
		this.address = address;
		this.dateOfBirth = dateOfBirth;
		this.covidVaccinated = covidVaccinated;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}
//
//	public List<Booking> getList() {
//		return list;
//	}

	public void setList(List<Booking> list) {
		this.blist = list;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(long mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPassState() {
		return passState;
	}

	public void setPassState(String passState) {
		this.passState = passState;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public short getCovidVaccinated() {
		return covidVaccinated;
	}

	public void setCovidVaccinated(short covidVaccinated) {
		this.covidVaccinated = covidVaccinated;
	}

	@Override
	public String toString() {
		return "Passenger [id=" + id + ", title=" + title + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", mobileNo=" + mobileNo + ", email=" + email + ", country=" + country + ", passState=" + passState
				+ ", city=" + city + ", address=" + address + ", dateOfBirth=" + dateOfBirth + ", covidVaccinated="
				+ covidVaccinated + "]";
	}

	

	
}
