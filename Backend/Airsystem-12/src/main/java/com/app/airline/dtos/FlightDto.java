package com.app.airline.dtos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

import com.app.airline.pojos.Schedule;
import com.fasterxml.jackson.annotation.JsonFormat;
@Component
public class FlightDto {
	private static final String MY_TIME_ZONE="Asia/Kolkata";

	private int flightId;
	private String airlineName;
	private String departureAirportName;
	private String arrivalAirportName;
	private String sourceCity;
	private String destinationCity;
	private double businessClassFare;
	private double economyClassFare;
	private int businessClassCapacity;
	private int economyClassCapacity;
	private int flightTotalCapacity;
	@Temporal(TemporalType.DATE)
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
	private Date departureDate;
	@Temporal(TemporalType.TIME)
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="HH:mm:ss",timezone = "Asia/Kolkata")
	private Date departureTime;
	@Temporal(TemporalType.DATE)
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
	private Date arrivalDate;
	@Temporal(TemporalType.TIME)
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="HH:mm:ss",timezone = MY_TIME_ZONE)
	private Date arrivalTime;
	private String flightStatus;
	private List<Schedule> scheduleList;
	public FlightDto() {
		this.scheduleList=new ArrayList<Schedule>();
	}
	public FlightDto(int flightId, String airlineName, String departureAirportName, String arrivalAirportName,
			String sourceCity, String destinationCity, double businessClassFare, double economyClassFare,
			int businessClassCapacity, int economyClassCapacity, int flightTotalCapacity, Date departureDate,
			Date departureTime, Date arrivalDate, Date arrivalTime, String flightStatus) {
		super();
		this.flightId = flightId;
		this.airlineName = airlineName;
		this.departureAirportName = departureAirportName;
		this.arrivalAirportName = arrivalAirportName;
		this.sourceCity = sourceCity;
		this.destinationCity = destinationCity;
		this.businessClassFare = businessClassFare;
		this.economyClassFare = economyClassFare;
		this.businessClassCapacity = businessClassCapacity;
		this.economyClassCapacity = economyClassCapacity;
		this.flightTotalCapacity = flightTotalCapacity;
		this.departureDate = departureDate;
		this.departureTime = departureTime;
		this.arrivalDate = arrivalDate;
		this.arrivalTime = arrivalTime;
		this.flightStatus = flightStatus;
	}
	
	
	public FlightDto(String sourceCity, String destinationCity, Date departureDate) {
		this.sourceCity = sourceCity;
		this.destinationCity = destinationCity;
		this.departureDate = departureDate;
	}
	public int getFlightId() {
		return flightId;
	}
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getDepartureAirportName() {
		return departureAirportName;
	}
	public void setDepartureAirportName(String departureAirportName) {
		this.departureAirportName = departureAirportName;
	}
	public String getArrivalAirportName() {
		return arrivalAirportName;
	}
	public void setArrivalAirportName(String arrivalAirportName) {
		this.arrivalAirportName = arrivalAirportName;
	}
	public String getSourceCity() {
		return sourceCity;
	}
	public void setSourceCity(String sourceCity) {
		this.sourceCity = sourceCity;
	}
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	public double getBusinessClassFare() {
		return businessClassFare;
	}
	public void setBusinessClassFare(double businessClassFare) {
		this.businessClassFare = businessClassFare;
	}
	public double getEconomyClassFare() {
		return economyClassFare;
	}
	public void setEconomyClassFare(double economyClassFare) {
		this.economyClassFare = economyClassFare;
	}
	public int getBusinessClassCapacity() {
		return businessClassCapacity;
	}
	public void setBusinessClassCapacity(int businessClassCapacity) {
		this.businessClassCapacity = businessClassCapacity;
	}
	public int getEconomyClassCapacity() {
		return economyClassCapacity;
	}
	public void setEconomyClassCapacity(int economyClassCapacity) {
		this.economyClassCapacity = economyClassCapacity;
	}
	public int getFlightTotalCapacity() {
		return flightTotalCapacity;
	}
	public void setFlightTotalCapacity(int flightTotalCapacity) {
		this.flightTotalCapacity = flightTotalCapacity;
	}
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	public Date getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public Date getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getFlightStatus() {
		return flightStatus;
	}
	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}
	public List<Schedule> getScheduleList() {
		return scheduleList;
	}
	public void setScheduleList(List<Schedule> scheduleList) {
		this.scheduleList = scheduleList;
	}
	@Override
	public String toString() {
		return "FlightDto [flightId=" + flightId + ", airlineName=" + airlineName + ", departureAirportName="
				+ departureAirportName + ", arrivalAirportName=" + arrivalAirportName + ", sourceCity=" + sourceCity
				+ ", destinationCity=" + destinationCity + ", businessClassFare=" + businessClassFare
				+ ", economyClassFare=" + economyClassFare + ", businessClassCapacity=" + businessClassCapacity
				+ ", economyClassCapacity=" + economyClassCapacity + ", flightTotalCapacity=" + flightTotalCapacity
				+ ", departureDate=" + departureDate + ", departureTime=" + departureTime + ", arrivalDate="
				+ arrivalDate + ", arrivalTime=" + arrivalTime + ", flightStatus=" + flightStatus + "]";
	}
		
	
	

}
