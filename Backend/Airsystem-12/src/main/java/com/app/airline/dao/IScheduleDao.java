package com.app.airline.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.airline.dtos.FlightScheduleDto;
import com.app.airline.pojos.Schedule;

public interface IScheduleDao extends JpaRepository<Schedule, Integer>{
	
//	@Query("select new com.app.airline.dtos.FlightScheduleDto(f.flightId, f.airlineName, s.departureTime, s.arrivalTime, CONCAT(MOD(HOUR(TIMEDIFF(s.arrivalTime,s.departureTime)), 24), ' hr ',MINUTE(TIMEDIFF(s.arrivalTime,s.departureTime)), ' min '), f.businessClassFare, f.economyClassFare) from Flight f join f.scheduleList s where f.flightId=1")
//	public List<FlightScheduleDto> getFlightScheduleDetails();

//	@Query("select new com.app.airline.dtos.FlightScheduleDto(f.flightId, f.airlineName, s.departureTime, s.arrivalTime, CONCAT(MOD(HOUR(TIMEDIFF(s.arrivalTime,s.departureTime)), 24), ' hr ',MINUTE(TIMEDIFF(s.arrivalTime,s.departureTime)), ' min '), f.businessClassFare, f.economyClassFare) from Flight f "
//			+ "join f.scheduleList s where f.destinationCity=?1 and f.sourceCity=?2 and s.departureDate=?3")
//	public List<FlightScheduleDto> getFlights(String destinationCity, String sourceCity, Date departureDate);
	
	@Query("select new com.app.airline.dtos.FlightScheduleDto(f.flightId, f.airlineName, s.scheduleId, s.departureTime, s.arrivalTime, CONCAT(MOD(HOUR(TIMEDIFF(s.arrivalTime,s.departureTime)), 24), ' hr ',MINUTE(TIMEDIFF(s.arrivalTime,s.departureTime)), ' min '), f.businessClassFare, f.economyClassFare) from Flight f "
			+ "join f.scheduleList s where f.destinationCity=?1 and f.sourceCity=?2 and s.departureDate=?3")
	public List<FlightScheduleDto> getFlights1(String destinationCity, String sourceCity, Date departureDate);
	
	Schedule findByScheduleId(int id);
	
	
	
}
