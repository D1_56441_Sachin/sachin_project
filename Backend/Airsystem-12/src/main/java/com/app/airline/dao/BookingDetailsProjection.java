package com.app.airline.dao;

import java.sql.Date;
import java.sql.Time;

public interface BookingDetailsProjection  {
	
	Integer getBooking_id();
	String getTicket_status();
	Integer getSeat_no();
	Double getTotal_fare();
	Date getBooking_date();
	Integer getFlight_id();
	String getSource_city();
	String getDestination_city();
	String getDeparture_airport_name();
	String getArrival_airport_name();
	Time getArrival_time();
	Time getDeparture_time();
	Date getArrival_date();
	Date getDeparture_date();
	String getFirst_name();
	String getLast_name();

}
