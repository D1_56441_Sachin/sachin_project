
import Content4 from "../Components/Content4";
import Footer from "../Components/Footer";
import HeaderSelector from "../Components/HeaderSelector";

// import "../Components/travelInfo.css";

const styles = {
    heading: {
        fontWeight:"bold",
        fontSize:'25px', 
        padding:'20px', 
        marginLeft:'15px'
        
    },

    content: {
        fontSize:'15px',
        marginLeft:'45px'
}


}


const TravelInfo = () => {


    return (
        <div>
            <HeaderSelector />
            {/* <Content2/>
    <Content3 /> */}
            {/* <Content4/> */}

            <div style={{backgroundColor:'#E5E4E2'}} >
                <br />
                <h1
                    style={{ textAlign: "center", fontWeight:'bolder'}}>
                    BEFORE YOU FLY
                </h1>
                <div>
                    <div style={styles.heading}>CHECK IN ONLINE</div>
                    <div style={styles.content}>Check in online to reduce interactions at the airport. Download your boarding pass to your phone through the Book My Flight website or print your boarding pass at home.</div>
                    <hr/>

                    <div style={styles.heading}>DIGITAL SERVICES </div>
                    <div style={styles.content}>
                        Get all the most important things done for your upcoming trip while you’re still at home by using our digital services and feel more relaxed when travelling on the day of your trip.
                    </div>
                </div>
                <hr/>

                <div style={styles.heading}> MEDICAL CARE IN AIR </div>              
                
                    <div style={styles.content}>
                    Medical incidents in the air, though rare, are always a possibility. for this our cabin crew is trained in advanced first aid and life support. our medical kits are recognized and approved as per international medical guidelines, which can be administered by our cabin crew in an eventuality and also by a doctor or nurse, who may be on board, willing to volunteer to assist if the situation demands
                    </div>
                    <hr/>

                <div style={styles.heading}>
                    TERMS AND CONDITIONS
                </div>
                <div style={styles.content}>
                    Valid for any Book My Flight tickets purchased either directly from Bookmyflight4.com or through travel agents. If you have booked your ticket through a travel agent, please contact them directly.
                    Book My Flight is entitled to change its flexibility policy with immediate effect and without prior notice.
                    <br/>  
                    <br/>                   

                    Keep your fully unutilized ticket open for two years from original ticket issue date (the date the first booking was made) and apply the value towards future travel.
                    <br/>  
                    <br/>                   

                    Date and route changes should be made prior to the scheduled departure time that you are booked on. If you have booked directly with Book My Flight, you may do this through our website or Contact Centres. If you have booked through a travel agency, please contact them directly.
                    <br/> 
                    <br/>                   

               

                    Rebook into the same booking class (RBD) and ticketed fare basis as the original ticket.Any difference in fare, taxes, fees, charges and surcharges will apply.
                    <br/> 
                    <br/>                   


                    If your travel is beyond 30 September 2022, ticketed fare basis conditions apply including any difference in fare, taxes, fees, charges and surcharges, and any applicable rebooking and no-show fees.
                    <br/> 
                    <br/>                   



                    Refund to the original form of payment.
                    <br/> 
                    <br/>                   


                    Refund requests must be made prior to the scheduled departure time that you are booked on. If you have booked directly with Qatar Airways, you may do this through our website by clicking here. If you have booked through a travel agency, please contact them directly.
                    <br/> 
                    <br/>                   


                    If you have booked directly with us, you may refund unutilized refundable value provided the refund is requested within 2 years from the date of issue of the document number presented for refund. If you have booked through a travel agency, please contact the travel agency on the refund deadline.
                    <br/> 
                    <br/>                   


                    
                    Refund amount will be credited using the original form of payment used to purchase the ticket.
                    <br/>                   
                    <br/>                   
                    

                </div>
            </div>
            <Content4 />
            <Footer />
        </div>
    );
}

export default TravelInfo