import AdminHeader from "../../Components/AdminHeader"
import Content2 from "../../Components/Content2"
import Content3 from "../../Components/Content3"
import Footer from "../../Components/Footer"


const AdminHome = () =>{
    const currentLoggedInUser = sessionStorage['firstName']
    return (
    <div style={{height: "1500px", backgroundRepeat : "no-repeat"}}>
        <AdminHeader/>
        <Content2 />
        <Content3 />
        <Footer/>
    </div>
    )
}
export default AdminHome