import { useState } from "react"
import { URL } from "../../config"
import axios from "axios"
import { toast } from "react-toastify"
import { useNavigate } from "react-router"
import HeaderSelector from "../../Components/HeaderSelector"
import Footer from "../../Components/Footer"
import Content4 from "../../Components/Content4"


const AddPackage = () => {

    const navigate = useNavigate()
    const [seatType, setSeatType] = useState('')
    const [food, setFood] = useState('')
    const [beverages , setBeverages] = useState('')
    const [baggage , setBaggage] = useState('')
    const [packageFare, setPackageFare] = useState(0)
    const [packageName, setPackageName] = useState('')

function AddNewPackage(){
    console.log(seatType)
  if (food.length == 0) {
        toast.warning('Please enter food')
      }else if (beverages.length == 0) {
        toast.warning('Please enter beverages')
    }else if (baggage.length == 0) {
        toast.warning('Please enter baggage')
    }else if (packageFare <= 0) {
        toast.warning('Please enter package fare')
      }else {
        const body = {
          packageName,
          seatType,
          food,
          beverages,
          baggage,
          packageFare,
        }
  
        // url to call the api
        const url = `${URL}/add`
  
        // http method: post
        // body: contains the data to be sent to the API
        axios.post(url, body).then((response) => {
          // get the data from the response
          const result = response.data
          console.log(result)
          if (result['status'] == 'success') {
            toast.success('Successfully added new package')
  
            // navigate to the signin page
            navigate('/editpackage')
          } else {
            toast.error(result['error'])
          }
        })
      }
}

    return (
        <div style={{backgroundColor:'#E5E4E2'}}>
            <HeaderSelector/>    
           <div >
            <br/>
            <br/>
            <h3 style={{backgroundColor:"#5C0632",color:"white"}}>Add package here</h3>
            <hr/>
  <div className="form-group row" style={{marginLeft:"50px"}}>
    
    <div>
     <label htmlFor="seattype" className="col-sm-2 col-form-label" style={{fontWeight:"bold"}}>Seat type</label>
      <input type="radio" name="seattype" value="economy" style={{width:"30px"}} onChange={(e) => {
        setSeatType(e.target.value)
        setPackageName('Regular')
      }}></input>Economy
      <input type="radio" name="seattype" value="business" style={{width:"30px"}} onChange={(e) => {
        setSeatType(e.target.value)
        setPackageName('Premium')
      }}></input>Business
    </div>
  </div>
  <div className="form-group row" style={{marginLeft:"50px"}}>
    <label htmlFor="food" className="col-sm-2 col-form-label" style={{fontWeight:"bold"}}>Food</label>
    <div className="col-sm-10">
      <input type="text" className="form-control" id="food" 
       onChange={(e) => {
        setFood(e.target.value)
      }} 
      placeholder="" style={{width:"500px"}} />
    </div>
  </div>
  <div className="form-group row" style={{marginLeft:"50px"}}>
    <label htmlFor="beverages" className="col-sm-2 col-form-label" style={{fontWeight:"bold"}}>Beverages</label>
    <div className="col-sm-10">
      <input type="text" className="form-control" id="beverages" 
       onChange={(e) => {
        setBeverages(e.target.value)
      }} 
      placeholder="" style={{width:"500px"}} />
    </div>
  </div>
  <div className="form-group row" style={{marginLeft:"50px"}}>
    <label htmlFor="baggage" className="col-sm-2 col-form-label" style={{fontWeight:"bold"}}>Baggage</label>
    <div className="col-sm-10">
      <input type="text" className="form-control" id="baggage" 
       onChange={(e) => {
        setBaggage(e.target.value)
      }} 
      placeholder=""  style={{width:"500px"}}/>
    </div>
  </div>
  <div className="form-group row" style={{marginLeft:"50px"}}>
    <label htmlFor="baggagefare" className="col-sm-2 col-form-label" style={{fontWeight:"bold"}}>Package fare</label>
    <div className="col-sm-10">
      <input type="text" className="form-control" id="baggagefare" 
       onChange={(e) => {
        setPackageFare(e.target.value)
      }} 
      placeholder=""  style={{width:"500px"}}/>
    </div>
  </div>
  <br/>
  <div className="form-group row" style={{marginLeft:"450px"}}>
    <div >
      <button type="submit" className="btn btn-warning btn-lg" onClick={AddNewPackage}>Add Package</button>
    </div>
  </div>

            </div>
            <Content4/>
          <Footer/> 
        </div>
    )
}

export default AddPackage