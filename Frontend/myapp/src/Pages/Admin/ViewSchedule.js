import { useEffect, useState } from "react"
import { URL } from "../../config"
import axios from "axios"
import { toast } from "react-toastify"
import { useLocation, useNavigate } from "react-router"
import HeaderSelector from "../../Components/HeaderSelector"
import Footer from "../../Components/Footer"
import Content4 from "../../Components/Content4"

const styles = {
    table:{
        fontWeight:'bold'  
    }
}

const Schedule = () => {
    const [ schedules, setSchedules] = useState([])
    const {state} = useLocation()
    const {flight} = state
    const [scheduleList, setScheduleList] = useState([])
const navigate = useNavigate()


function ViewSchedule(){

    const url=`${URL}/admin/flight/${flight.flightId}`
    axios.get(url).then( (response) => {
        const result=response.data
        console.log(result)
        const schedulelist=response.data.data.scheduleList
        console.log(schedulelist)
        if(result['status']=='success' && schedulelist != 0){
            setScheduleList(schedulelist)
                toast.success('Checkout Schedules')       
        }else{
            toast.error('No schedule available for this flight')
            navigate('/editflight') 
        }

    })
}

useEffect(ViewSchedule,[])

    return (
        <div>
            <HeaderSelector/>
            <div  style={{backgroundColor:'#E5E4E2'}}>
          <br/>
            <h4 style={{textAlign:"center"}}>Schedules for flight : {flight.airlineName}</h4>
            {
                scheduleList.map( (s) => (
                <table style={{ borderBlockStyle:'groove'}}
                 className="table table-bordered table-hover table-warning">
                     <thead>
                     <button className="btn btn-warning "
                        onClick={()=>{navigate('/editschedule',{state:{flightschedule :s}})}} >Edit Schedule</button>
                     </thead>
                    <tbody>
                        <tr>
                            <td style={styles.table}>Flight ID</td>
                            <td>{flight.flightId}</td>
                        </tr>
                        <tr>
                            <td style={styles.table}>Schedule ID</td>
                            <td>{s.scheduleId}</td>
                        </tr>
                        <tr>
                            <td style={styles.table}>Departure Date</td>
                            <td>{s.departureDate}</td>
                        </tr>
                        <tr>
                            <td style={styles.table}>Departure Time</td>
                            <td>{s.departureTime}</td>
                        </tr>
                        <tr>
                            <td style={styles.table}>Arrival date</td>
                            <td>{s.arrivalDate}</td>
                        </tr>
                        <tr>
                            <td style={styles.table}>Arrival Time</td>
                            <td>{s.arrivalTime}</td>
                        </tr>
                        <tr>
                            <td style={styles.table}>Flight Status</td>
                            <td>{s.flightStatus}</td>
                        </tr>
                    </tbody>
                </table>
                
                ))
            }
             </div>
             <Content4/>
             <Footer/>
        </div>
    )
}

export default Schedule