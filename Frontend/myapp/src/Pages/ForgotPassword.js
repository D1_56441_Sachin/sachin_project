import axios from "axios"
import { useState } from "react"
import { useNavigate } from "react-router"
import { toast } from "react-toastify"
import Footer from "../Components/Footer"
import HeaderSelector from "../Components/HeaderSelector"
import { URL } from "../config"

const ForgotPassword = () => {

    const [to, setTo] = useState('')
    const [subject, setSubject] = useState('OTP for forgot password')
    const [message, setMessage] = useState('Please enter the following OTP for resetting your forgotten password : ')
    const navigate = useNavigate()

function resetPass(){
if(to.length == 0){
    toast.warning('email can not be empty')
}else{
    const body= {
    to,
    subject,
    message,
    }

    const url = `${URL}/users/sendmail`
    axios.post(url, body).then( (response) => {
        const result = response.data
        console.log(result)
        if(result['status'] == 'success')
        {
            toast.success('Password reset OTP is sent to email')
            navigate('/otpverify', { state: { otpToVerify: result.data, emailto: to } })
        }else{
        toast.error(result['error'])
        }
    })
}
}

    return (
        <div style={{"width":'100%',"height":'100%',backgroundColor:'#E5E4E2'}}>
          <HeaderSelector/>
          <div  style={{height:'550px', backgroundColor:'#E5E4E2'}}>
              <div className="row">
                <div className="col"></div>
                    <div className="col">
                        <div className="form">
                            <div className="mb-3" style={{marginTop:'100px'}}>
                                <label htmlFor="email" className="label-control">
                                    <h3 style={{color:"#5C0632"}}>Enter your registered email</h3></label>
                                <br></br>
                                    <div class="alert alert-success bg-soft-primary border-0" role="alert" >
                                        Enter your email address and we'll send you an email with OTP to reset your password.
                                    </div>
                                <input onChange={(e) => {
                                    setTo(e.target.value)
                                }
                                }
                                    type="email" id="email" placeholder="abc@gmail.com" 
                                    style={{width:'400px', height:"40px"}} >
                                </input>
                                <br/>
                                <br/>
                                <button onClick={resetPass}
                                    className="btn btn-warning" style={{width:'400px',marginTop:'30px', fontWeight:"bold"}}>
                                    Send Resest Link
                                </button>
                            </div>

                        </div>
                    </div>
                <div className="col"></div>
              </div>
          </div> 
          <Footer/>   
        </div>
    )
}

export default ForgotPassword