import { useState } from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import axios from 'axios'
import { useNavigate } from 'react-router'
import {URL} from '../config';
import Header from '../Components/Header'
import Footer from '../Components/Footer'

const styles={
    div:{
        backgroundColor:'#E5E4E2',
        width:'100%',
        height:'100%'
    }
}

const Login = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const navigate = useNavigate()

  const signinUser = () => {
    if (email.length == 0) {
      toast.warning('please enter email')
    } else if (password.length == 0) {
      toast.warning('please enter password')
    } else {
      const body = {
        email,
        password,
      }
      
      // url to make signin api call
      const url = `${URL}/users/signin`

      // make api call using axios
      axios.post(url, body).then((response) => {
        // get the server result
        const result = response.data
        console.log(result['data'])
        if (result['status'] == 'success') {
          toast.success('Welcome to the ARS')

          // get the data sent by server
          const { id, firstName, lastName, email, role } = result['data']

          // persist the logged in user's information for future use
          sessionStorage['userId'] = id
          sessionStorage['firstName'] = firstName
          sessionStorage['lastName'] = lastName
          sessionStorage['email'] = email
          sessionStorage['loginStatus'] = 1
			    sessionStorage['role'] = role
          // navigate to home component
          navigate('/home')
        } else {
          toast.error('Invalid user name or password')
        }
      })
    }
  }

  return (
    <div className='row d-flex' style={{"width":'100%',"height":'100%',backgroundColor:'#E5E4E2'}}>
        <Header/>
      

      <div className='row d-flex'>
        {/* <div className="col"></div> */}
        <div className='row d-flex' style={{marginLeft:'350px',width:'500px', height:'545px',backgroundColor:'#E5E4E2'}}>
        <h1 style={{marginTop:'100px'}}>Login</h1>
          <div className="form" style={{marginTop:'25px'}}>
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Email address
              </label>
              <input
                onChange={(e) => {
                  setEmail(e.target.value)
                }}
                type="text"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Password
              </label>
              <input
                onChange={(e) => {
                  setPassword(e.target.value)
                }}
                type="password"
                className="form-control"
              />
            </div>

            <div>
              <div >
                <Link to="/forgotpassword" style={{color:'#5C0632'}}>Forgotten password?</Link>
              </div>
                <button onClick={signinUser} className="btn btn-primary" style={{marginTop:'20px', width:'500px',backgroundColor:'#5C0632'}}>
                Signin
              </button>
            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
      <Footer/>
    </div>
  )
}

export default Login