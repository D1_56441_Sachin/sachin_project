

import Content4 from "../Components/Content4";
import Footer from "../Components/Footer";
import HeaderSelector from "../Components/HeaderSelector";



const styles = {
    heading: {
        fontWeight: "bold",
        fontSize: '20px',
        padding: '10px',
        marginLeft: '15px'

    },

    content: {
        fontSize: '15px',
        marginLeft: '20px'
    }


}


const FAQs = () => {


    return (
        <div>
            <HeaderSelector />
            {/* <Content2/>
    <Content3 /> */}
            {/* <Content4/> */}

            <div style={{ backgroundColor: '#E5E4E2' }} >
                <br />
                <h1
                    style={{ textAlign: "center", fontWeight: 'bolder' }}>
                    FAQs
                </h1>
                <div>
                    <div style={styles.heading}>1) Is a negative RT-PCR report required for all domestic flights?</div>
                    <div style={styles.content}>Government mandates for each state are varying and updated on a frequent basis. It is therefore advised for you to check the latest State-wise guidelines before travel.
                    </div>
                    <hr />

                    <div style={styles.heading}>2) Can I fly if my RT-PCR report hasn't arrived before my flight time?</div>
                    <div style={styles.content}>If your destination city/state mandates a negative RT-PCR report, then you must carry the same to be allowed boarding from your origin station.
                    </div>
                    <hr />

                    <div style={styles.heading}>3) How can I track my flight status?</div>
                    <div style={styles.content}>To know the status of your flight, go to Flight Status, enter your flight details and click on search button.
                    </div>
                    <hr />

                    <div style={styles.heading}>4) Is it mandatory to wear a mask during travel?</div>
                    <div style={styles.content}>Yes, it is compulsory to wear a mask at all times throughout your journey, be it at the airport or inside the flight. It helps you, your fellow passengers and us stay safe.
                    </div>
                    <hr />


                    <div style={styles.heading}>5) Is it mandatory to wear a mask during travel?</div>
                    <div style={styles.content}>Yes, it is compulsory to wear a mask at all times throughout your journey, be it at the airport or inside the flight. It helps you, your fellow passengers and us stay safe.
                    </div>
                    <hr />

                    <div style={styles.heading}>6) Can people who have recently recovered from COVID-19 travel?</div>
                    <div style={styles.content}>Yes, people who have recovered from COVID-19 can safely travel if they have met criteria to travel.
                    <br/><br/>
                    Travelers who have recovered from COVID-19 in the past 90 days do not need to get tested before or after travel. If they develop COVID-19 symptoms before, during or after travel, they should isolate and consult with a healthcare provider for testing recommendations.
                    <br/><br/>
                    Those traveling by air to the United States from a foreign country can show documentation of recovery from COVID-19 instead of a negative test result before boarding their flight.
                    <br/><br/>
                    These travelers should follow other travel recommendations and requirements for domestic and international travel.
                    
                    </div>
                    <hr />

                    <div style={styles.heading}>7) How can I protect myself from COVID-19 when traveling in flight?</div>
                    <div style={styles.content}>Maintaining physical distance to prevent COVID-19 is often difficult on public transportation. People may not be able to keep a distance of 6 feet from others on airplanes, trains, or buses.
                    <br/><br/>

                    Protect yourself from COVID-19 by staying up to date with your COVID-19 vaccines and by wearing a mask on public transportation. Wearing a mask over your nose and mouth is required on in indoor areas of planes, buses, trains, and other forms of public transportation traveling into, within, or out of the United States, and while indoors at U.S. transportation hubs (such as airports and stations), of your vaccination status.
                    <br/><br/>
                    All travelers should take steps to protect themselves and others.
                    </div>
                    <hr />

                    <div style={styles.heading}>8) What time does boarding close for my flight?</div>
                    <div style={styles.content}>Boarding gate closes 20 minutes prior to flight departure.
                    </div>
                    <br/>
                  







                </div>
            </div>
            <Content4 />
            <Footer />
        </div>
    );
}

export default FAQs