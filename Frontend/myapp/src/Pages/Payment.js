import axios from "axios"
import { useEffect, useState } from "react"
import { useLocation, useNavigate } from "react-router"
import HeaderSelector from "../Components/HeaderSelector"
import { URL } from "../config"
import {toast} from 'react-toastify'
import Footer from "../Components/Footer"

const Payment=()=>{
    const navigate= useNavigate()
    const {state}=useLocation()
    const{totalFare}=state
    //  const[totalFare, setTotalFare]= useState(4500)
     const [cardNo1, setCardNo1]=useState('')
    
    const[cardId, setCardId]=useState('')
    const [cardList, setCardList]=useState([])
    const[actualCvv, setActualCvv]=useState("")
    const[enteredCvv, setEnteredCvv]=useState("")
    const [expiryDate, setExpiryDate] = useState("")
    const[emailForPaymentOtp, setEmailForPaymentOtp]=useState("")
    const userId=sessionStorage.getItem('userId')
    const flightId=sessionStorage.getItem('flightId')
    const offerId=sessionStorage.getItem('offerId')
    const scheduleId=sessionStorage.getItem('scheduleId')
    const passengerId=sessionStorage.getItem('pId')
    const selectedPackageName=sessionStorage.getItem('selectedPackageName')
    const selectedPackageFare=sessionStorage.getItem('selectedPackageFare')
    const currentDate = new Date();
    const bookingDate = currentDate.getFullYear() + "-0" + (currentDate.getMonth() + 1) + "-" + currentDate.getDate();
    //const expiryDate1 = expiryDate.getFullYear() + "-" + (expiryDate.getMonth() + 1) + "-" + expiryDate.getDate();
    console.log(userId)
    console.log(expiryDate)
    console.log(bookingDate)
    console.log(typeof(expiryDate))
    console.log(typeof(bookingDate))
    console.log(currentDate)
    // const expiryDate1=expiryDate.toString()
    // console.log(expiryDate1)
    useEffect(() => {
        const getCardList = () => {

          const url = `${URL}/paymentcards/findByUserId/${userId}`;
        //  const url = `${URL}/paymentcards/findByUserId/13`;
        //   const url = `${URL}/paymentcards/getAllCards`;
          axios.get(url).then((response) => {
              const result=response.data
            setCardList(result['data'])
            // setCardNo1(cardList[0].cardNo)
            console.log(response)
            
          });
        };
        getCardList();
      }, []);
      
    //   console.log(cardNo1)
    //   let cNo=parseInt(cardList[0].cardNo)
    // //    let cardStart= cNo.toString().substring(0,4)
     
    // //  console.log(cardStart)
    // // let cardEnd=cNo.toString().substring(8,13)
    // // console.log(cardEnd)
    // console.log(cNo.toString())
      const PayNow=()=>{
          if(enteredCvv.length < 3){
              toast.warning("Enter Correct CVV")
          }else if(actualCvv == enteredCvv){
            toast.success("CVV verified")
            if(expiryDate > bookingDate){
            const body = {
                passengerId,
                scheduleId,
                userId,
                flightId,
                bookingDate,
                totalFare
              }
              const url=`${URL}/user/booking/addBooking`
              axios.post(url,body).then((response)=>{
                  const result=response.data
                  if(response){
                      const bookingId=result.bookingId;
                      sessionStorage['bookinngId']=bookingId
                    console.log(bookingId)
                    navigate("/user/ticket",{state:{bookingId : bookingId}})
                  }else {
                    toast.error('error')
                  }
              })
            }else
              toast.warning("Payment Card expired")
          }else{
              toast.error("Incorrect CVV Please Enter correct CVV")
              navigate("/user/payment")
          }
      }
      
      
return(
    <div style={{backgroundColor:'#E5E4E2', height:'100%'}}>
        <HeaderSelector/>
        {/* <div className="col mt-4 d-flex justify-content-center">
        <h3>Make Payment of 
          <span style={{color:'red'}}> Rs. {totalFare} </span>
          </h3>
        </div> */}
        <div class="container-xl mt-5 mb-5 d-flex justify-content-center " style={{width:'1100px    '}}>
    <div class="card p-5">
        <div>
            <h4 class="heading">Make a payment</h4>
            <p class="text">Please make the payment to confirm your seat</p>
            <div class="col mt-3 d-flex justify-content-end"> <button class="btn btn-primary btn-block payment-button " onClick={()=>{navigate('/addCard')}}> Add Card </button> </div>
        </div>
        <div class="pricing p-3 rounded mt-4 d-flex justify-content-between">
            {/* <div class="images d-flex flex-row align-items-center"> <img src="https://i.imgur.com/S17BrTx.png" class="rounded" width="60"/></div> */}
                <div class="d-flex flex-column ml-4"> <span class="business">Total Fare</span> 
            </div>
            {/* <!--pricing table--> */}
            
            <div class="d-flex flex-row align-items-center"> <sup class="dollar font-weight-bold"></sup> <span class="amount ml-1 mr-1"> &#x20b9; {totalFare}</span> </div> 
        </div> <span class="detail mt-5">Payment details</span>
        
        <div class="credit rounded mt-4 d-flex justify-content-between align-items-center">
            <table>
                <tbody>
            {
                cardList.map((card)=>(
                    <tr key={card.cardNo}> 
                    <div class="row">
                    {/* <div class=" col d-flex flex-row align-items-center" style={{width:'700px'}}> <img src="https://i.imgur.com/qHX7vY1.png" class="rounded" width="70"/>
                <div class="d-flex flex-column ml-3"> <span class="business">Credit Card</span> <span class="plan">{cardStart} XXXX XXXX {cardEnd}</span> </div>
            </div> */}
            
            <div class=" col d-flex flex-row align-items-center" style={{width:'500px'}}><img src={require("../Components/images/card.png")} class="rounded" width="70"/>
                {/* <div class="d-flex flex-column ml-3"> <span class="business">Credit Card</span> <span class="plan">{cardStart} XXXX XXXX {cardEnd}</span> </div> */}
                <div class="d-flex flex-column ml-3"> <span class="business">Credit Card</span> <span class="plan">{card.cardNo}</span> </div>
                </div>
            
            {/* <div class=" col  align-items-center" style={{width:'50px',marginLeft:'75px'}}><label for="" class="form__label">Expiry Date</label> <input type="text" class="form-control" placeholder="MM / yy" style={{width:"125px"}}/>  </div> */}
            <div class=" col  align-items-center" style={{width:'100px',marginLeft:'120px'}}><label for="" class="form__label">CVV Code</label> <input type="text" id={card.cardId} class="form-control cvv" placeholder="CVV" maxlength="3" style={{width:"125px"}}
            onChange={(e)=>{setEnteredCvv(e.target.value);setActualCvv(card.cvv);setExpiryDate(card.validityDate)}}
            /> </div>
             
            
                                      
            </div>
            </tr>
                ))
            
}
</tbody>
</table>
        </div>
        {/* <div class="credit rounded mt-2 d-flex justify-content-between align-items-center">
            <div class="d-flex flex-row align-items-center"> <img src="https://i.imgur.com/qHX7vY1.png" class="rounded" width="70"/>
                <div class="d-flex flex-column ml-3"> <span class="business">Credit Card</span> <span class="plan">2344 XXXX XXXX 8880</span> </div>
            </div>
            <div> <input type="text" class="form-control cvv" placeholder="CVC"/> </div>
        </div> */}
        {/* <h6 class="mt-4 text-primary">ADD PAYMENT METHOD</h6>
        <div class="email mt-2"> <input type="text" class="form-control email-text" placeholder="Email Address" /> </div> */}
        <div class="row">
        <div class="col mt-3 d-flex justify-content-start"> <button class="btn btn-primary btn-block payment-button " onClick={()=>{navigate("/user/paymentDetails", {state:{selectedPackageFare:selectedPackageFare, selectedPackageName:selectedPackageName}})}}><i class="fa fa-long-arrow-left"></i> Back </button> </div>
        
        <div class="col mt-3 d-flex justify-content-end"> <button class="btn btn-primary btn-block payment-button " 
        onClick={PayNow}>Pay Now </button> </div>
        </div>
    </div>
</div>
<Footer/>
    </div>
)
}
export default Payment