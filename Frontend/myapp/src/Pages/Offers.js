import Header from "../Components/Header";
import { useEffect, useState } from "react";
import { URL } from "../config";
import axios from "axios";
import { toast } from "react-toastify";
import { useNavigate } from "react-router";
import AfterLoginHeader from "../Components/AfterLoginHeader";
import HeaderSelector from "../Components/HeaderSelector";
import Footer from "../Components/Footer";
import Content4 from "../Components/Content4";

const styles = {
  table: {
    fontWeight: "bold",
  },
};

const Offer = () => {
  const [offers, setOffers] = useState([]);
  const navigate = useNavigate();

  function ViewOffers() {
    const url = `${URL}/offers/`;
    axios.get(url).then((response) => {
      const result = response.data;
      console.log(result);
      if (result["status"] == "success") {
        setOffers(result["data"]);
        toast.success("Checkout Special offers");
      } else {
        toast.warning("No offer available now");
        navigate("/loginhome");
      }
    });
  }

  useEffect(ViewOffers, []);

  return (
    <div>
      <HeaderSelector />
      <h2>
        <marquee>We have some exciting offers for you..........!!!</marquee>
      </h2>
      <hr />
      {/*} <table className="table table-bordered table-hover table-warning">
                <thead>
                    <tr>
                        <th>Offer ID</th>
                        <th>Promocode</th>
                        <th>Discount</th>
                        <th>Min.Transaction amt.</th>
                        <th>Valid on</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        offers.map( (o) => (
                            <tr key={o.id}>
                               <td>{o.id}</td> 
                               <td>{o.promocode}</td>
                                <td>{o.discount}</td>
                                <td>{o.min_txn_amount}</td>
                                <td>{o.valid_on}</td>
                            </tr>
                        )    
                        )
                    }
                </tbody>
            </table>
                */}

      {offers.map((o) => (
        <table
          style={{ borderBlockStyle: "groove" }}
          className="table table-bordered table-hover table-warning"
        >
          <tbody>
            <tr>
              <td style={styles.table}>ID</td>
              <td>{o.id}</td>
            </tr>
            <tr>
              <td style={styles.table}>Promocode</td>
              <td>{o.promoCode}</td>
            </tr>
            <tr>
              <td style={styles.table}>Discount</td>
              <td>{o.discount}</td>
            </tr>
            <tr>
              <td style={styles.table}>Min.Txn amt</td>
              <td>{o.minTxnAmount}</td>
            </tr>
            <tr>
              <td style={styles.table}>Valid On</td>
              <td>{o.validOn}</td>
            </tr>
          </tbody>
        </table>
      ))}
      <Content4/>
      <Footer/>
    </div>
  );
};

export default Offer;
