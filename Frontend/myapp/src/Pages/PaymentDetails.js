import { useLocation, useNavigate } from "react-router";
import HeaderSelector from "../Components/HeaderSelector";
import CurrencyInput from 'react-currency-input-field';
import Footer from "../Components/Footer";
const styles = {
  td: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "485px",
  },
  td1: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
  },
  span: {
    fontWeight: "bold",
  },
  td2: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "40.80%",
  },
  td3: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "40%",
  },
  td4: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "39.65%",
  },
  td5: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "39.65%",
  },
  div: {
    marginLeft: "140px",
  },
};
const PaymentDetails = () => {
  const navigate = useNavigate()
   const { state } = useLocation()
    const { seatType, selectedPackageFare, selectedPackageName } = state;
    const selectedFlightId =sessionStorage.getItem('flightId')
    const selectedFlightFare =sessionStorage.getItem('fare')
    const selectedOfferId =sessionStorage.getItem('offerId')
    const selectedOfferDiscount =sessionStorage.getItem('offerDiscount')
    const totalBaggage=sessionStorage.getItem('totalBaggage')
    const extraBaggageAllowed=sessionStorage.getItem('extraBaggageAllowed')
    let extraBaggage= parseInt(totalBaggage) - parseInt(extraBaggageAllowed) - 5
    let extraBaggageCharge= parseInt(extraBaggage) * 50
    let totalFare = parseInt(selectedFlightFare) + parseInt(selectedPackageFare) +parseInt(extraBaggageCharge) - parseInt(selectedOfferDiscount)
    sessionStorage['totalFare']=totalFare
    console.log(extraBaggage)
  return (
    <div style={{backgroundColor:'#E5E4E2', height:'100%'}}>
        <HeaderSelector/>
        <div className="col mt-4 d-flex justify-content-center"><h1>Payment Details</h1></div>

      <div
        className="tab-pane fade active show"
        id="faq_tab_1"
        role="tabpanel"
        aria-labelledby="faq_tab_1-tab"
      >
        <div className="container p-3">
          <div style={styles.div}>
            <table>
                <tbody>
              <tr style={{ color: "white" }}>
                <td style={styles.td1}>
                  <label htmlFor="flightId">Flight Id</label>{" "}
                </td>
                <td style={styles.td1}>
                  <label htmlFor="flightfare">Flight Fare</label>
                </td>
              </tr>
              <tr>
                <td style={styles.td}>
                  <input 
                  type="text" 
                  className="form-control" 
                  id="flightId" 
                  value={selectedFlightId} 
                  readOnly
                  />
                </td>
                <td style={styles.td}>
                  
                <CurrencyInput 
                  intlConfig={{ locale: 'en-IN', currency: 'INR' }}
                  type="text" 
                  className="form-control" 
                  id="flightfare" 
                  value={selectedFlightFare} 
                  readOnly
                  />
                </td>
              </tr>
              </tbody>
            </table>
          </div>
          <div style={styles.div}>
            <table>
            <tbody>
              <tr style={{ color: "white" }}>
                <td style={styles.td1}>
                  <label htmlFor="packageName">Package Name</label>{" "}
                </td>
                <td style={styles.td1}>
                  <label htmlFor="offer">Offer</label>
                </td>
              </tr>
              <tr>
                <td style={styles.td}>
                  <input
                    value={selectedPackageName}
                    type="text"
                    className="form-control"
                    placeholder="Package Name"
                    readOnly
                  />
                </td>
                <td style={styles.td}>
                  <input 
                  value={selectedOfferId} 
                  type="text" 
                  className="form-control" 
                  placeholder="Offer"
                  readOnly
                   />
                </td>
              </tr>
              </tbody>
            </table>
          </div>
          <div style={styles.div}>
            <table>
            <tbody>
              <tr style={{ color: "white" }}>
                <td style={styles.td1}>
                  <label htmlFor="packageAmount">Package Amount</label>{" "}
                </td>
                <td style={styles.td1}>
                  <label htmlFor="offerDiscount">Offer Discount</label>
                </td>
              </tr>
              <tr>
                <td style={styles.td}>
                <CurrencyInput 
                  intlConfig={{ locale: 'en-IN', currency: 'INR' }}
                    value={selectedPackageFare}
                    type="text"
                    className="form-control"
                    placeholder="Package Amount"
                    readOnly
                  />
                </td>
                <td style={styles.td}>
                <CurrencyInput 
                  intlConfig={{ locale: 'en-IN', currency: 'INR' }}
                  value={selectedOfferDiscount}
                    type="text"
                    className="form-control"
                    placeholder="Offer Discount"
                    readOnly
                  />
                </td>
              </tr>
              </tbody>
            </table>
          </div>
          <div style={styles.div}>
            <table>
            <tbody>
              <tr style={{ color: "white" }}>
                <td style={styles.td1}>
                  <label htmlFor="extraBaggage">Extra Baggage</label>{" "}
                </td>
                <td style={styles.td1}>
                  <label htmlFor="baggageFare">Baggage Charge</label>
                </td>
              </tr>
              <tr>
                <td style={styles.td}>
                <CurrencyInput 
                  suffix=" Kg"
                  
                  value={extraBaggage}
                    type="text"
                    className="form-control"
                    placeholder="Extra Baggage"
                    readOnly
                  />
                </td>
                <td style={styles.td}>
                <CurrencyInput 
                  intlConfig={{ locale: 'en-IN', currency: 'INR' }}
                  value={extraBaggageCharge}
                    type="text"
                    className="form-control"
                    placeholder="Baggage Charge"
                    readOnly
                  />
                </td>
              </tr>
              </tbody>
            </table>
          </div>
          <div className="col mt-4 d-flex justify-content-center">
         
            <table>
            <tbody>
              <tr style={{ color: "white" }}>
                <td style={styles.td1}>
                  <label htmlFor="totalFare">Total Fare</label>{" "}
                </td>
               
              </tr>
              <tr>
                <td style={styles.td}>
                  <CurrencyInput 
                  intlConfig={{ locale: 'en-IN', currency: 'INR' }}
                  prefix="Rs.  "
                  value={totalFare}
                  type="text" 
                  className="form-control" 
                  id="totalFare" 
                  readOnly
                  />
                </td>
            </tr>
            </tbody>
            </table>
            
          </div>
          <div className="row">
          <div className=" col mt-4 d-flex justify-content-start">
            
            <button className="btn btn-primary custom-button px-5 justify-content-start" 
            style={{backgroundColor:'#5C0632'}}
            onClick={()=>{navigate('/user/selectPackage')}}>
              Back to Package Selection
            </button>
          </div>
          <div className="col mt-4 d-flex justify-content-end">
            
            <button className="btn btn-primary custom-button px-5 justify-content-end" 
            style={{backgroundColor:'#5C0632'}}
            onClick={()=>{navigate('/user/payment', {state:{totalFare:totalFare}})}}
            >
              Proceed to Payment
            </button>
          </div>
          </div>
        </div>
      </div>
      <Footer/>
    </div>
  );
};

export default PaymentDetails;
