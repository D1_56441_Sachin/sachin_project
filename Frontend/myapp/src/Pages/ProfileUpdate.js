import axios from "axios"
import { useState } from "react"
import { useNavigate } from "react-router"
import { toast } from "react-toastify"
import Footer from "../Components/Footer"
import HeaderSelector from "../Components/HeaderSelector"
import { URL } from "../config"

const ProfileUpdate = () => {

    const [ nameOnCard,setNameOnCard ] = useState('') 
    const [ cardNo, setCardNo ] = useState(0)
    const [ validityDate, setValidityDate ] = useState('')
    const [ cvv, setCvv ] = useState(0)
    const [ userId, setUserId ] = useState(sessionStorage.getItem('userId'))
    const [ firstName, setFirstName ] = useState('')
    const [ lastName, setLastName ] = useState('')

    const navigate = useNavigate()

    const updateProfile = () => {
        if(nameOnCard.length == 0){
        toast.warning('Please enter Name On Card')
        }else if (cardNo.length != 12){
            toast.warning('Please enter 12 digit card number')
        }else if (validityDate.length == 0){
            toast.warning('Please enter validity date')
        }else if (cvv.length != 3){
            toast.warning('Please enter 3 digit cvv')
        }else {
            const body = {
                nameOnCard,
                cardNo,
                validityDate,
                cvv,
                userId,    
            }

            const url = `${URL}/paymentcards/add`

            axios.post(url, body).then((response) => {
                const result = response.data
                console.log(result)
                if (result['status'] == 'success'){
                    toast.success('Added your card')

                    navigate('/profileUpdate')
                }else {
                    toast.error(result['error'])
                }
            })
        }

    }

    const saveUser = () => {
        if(firstName.length == 0){
            toast.warning('Please enter first Name')
            }else if (lastName.length == 0){
                toast.warning('Please enter last name')
            }else {
                const body = {
                    firstName,
                    lastName,
                }
    
                const url = `${URL}/users/${userId}`
    
                axios.put(url, body).then((response) => {
                    const result = response.data
                    console.log(result)
                    if (result['status'] == 'success'){
                        toast.success('Name changed succesfully')
                        sessionStorage['firstName']= firstName
                        sessionStorage['lastName']= lastName
                        navigate('/home')
                    }else {
                        toast.error(result['error'])
                    }
                })
            }
    }

    return (
        <div style={{"width":'100%',"height":'100%',backgroundColor:'#E5E4E2'}}>
           <HeaderSelector/>

        <div >
            
            <div className="row">
                     
            <div className="col">
                
            </div>
                    <div className="col">
                    <h3 className="title">Add Payment Card</h3>
                        <div className="form">
                            <div className="mb-3">
                             <label className="label-control">
                                 Name on card
                             </label>   
                             <input 
                             onChange={ (e) => {
                                 setNameOnCard(e.target.value)
                             }}
                             type="text"
                             className="form-control"
                             ></input>
                            </div>
                            
                            <div className="mb-3">
                             <label className="label-control">
                             Card number
                             </label>
                             <input
                             onChange={ (e) => {
                                 setCardNo(e.target.value)
                             }}
                             type="number"
                             className="form-control"
                             ></input>
                            </div>

                            <div className="mb-3">
                             <label className="label-control">
                             Valid till
                             </label>
                             <input
                             onChange={ (e) => {
                                 setValidityDate(e.target.value)
                             }}
                             type="date"
                             className="form-control"
                             ></input>
                            </div>

                            <div className="mb-3">
                             <label className="label-control">
                             CVV
                             </label>
                             <input
                             onChange={ (e) => {
                                 setCvv(e.target.value)
                             }}
                             type="number"
                             className="form-control"
                             ></input>
                            </div>
                            
                            <div className="mb-3">
                             <button onClick={updateProfile} className="btn btn-primary">Update</button>   
                            </div>
                        </div>
                    </div>
                <div className="col"></div>
                <div className="col">
                <h3 className="title">Name Change</h3>
                            <div className="mb-3">
                             <label className="label-control">
                             First Name
                             </label>
                             <input
                             onChange={ (e) => {
                                 setFirstName(e.target.value)
                             }}
                             type="text"
                             placeholder={sessionStorage.getItem('firstName')}
                             className="form-control"
                             ></input>
                            </div>

                            <div className="mb-3">
                             <label className="label-control">
                             Last Name
                             </label>
                             <input
                             onChange={ (e) => {
                                 setLastName(e.target.value)
                             }}
                             type="text"
                             placeholder={sessionStorage.getItem('lastName')}
                             className="form-control"
                             ></input>
                            </div>
                            
                            <div className="mb-3">
                             <button onClick={saveUser} className="btn btn-primary">Save</button>   
                            </div>
                </div>
                <div className="col"></div>
            </div>
        </div>
        <Footer/>
        </div>
    )
}

export default ProfileUpdate