import axios from "axios";
import { useEffect, useState } from "react"
import { useLocation, useNavigate } from "react-router";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import Footer from "../Components/Footer";
import HeaderSelector from "../Components/HeaderSelector"
import "../Components/ticket.css"
import { URL } from "../config";
import jsPDF  from "jspdf"

const TicketDetails= ()=>{
  var image = '<svg clip-rule="evenodd" fill-rule="evenodd" height="60" width="60" image-rendering="optimizeQuality" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg"><g stroke="#222"><line fill="none" stroke-linecap="round" stroke-width="30" x1="300" x2="55" y1="390" y2="390"/><path d="M98 325c-9 10 10 16 25 6l311-156c24-17 35-25 42-50 2-15-46-11-78-7-15 1-34 10-42 16l-56 35 1-1-169-31c-14-3-24-5-37-1-10 5-18 10-27 18l122 72c4 3 5 7 1 9l-44 27-75-15c-10-2-18-4-28 0-8 4-14 9-20 15l74 63z" fill="#222" stroke-linejoin="round" stroke-width="10"/></g></svg>';
  const navigate=useNavigate()
    //var result=JSON.parse(sessionStorage.getItem("bookingData"))
    const {state}=useLocation()
    const{bookingId}=state
    const [result, setData] = useState([]);
  useEffect(() => {
    const getBookingDetails = () => {
      const url = `${URL}/user/booking/getBookingDetailsById/${bookingId}`;
      // const url = `${URL}/user/booking/getBookingDetailsById/17`;
      axios.get(url).then((response) => {
        setData(response.data)
        
      });
    };
    getBookingDetails();
  }, []);
  console.log(result)
  var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thurs', 'Fri', 'Sat'];
  const monthNames = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN",
  "JUL", "AUG", "SEPT", "OCT", "NOV", "DEC"
];
  const currentDate = new Date(result.departure_date);
  const arDate = new Date(result.arrival_date);
  const departureDate = days[currentDate.getDay()] +" "+ monthNames[currentDate.getMonth()]+" " + currentDate.getDate()+" "+ currentDate.getFullYear();
 // console.log(departureDate)
  const arrivalDate = days[arDate.getDay()] +" "+ monthNames[arDate.getMonth()]+" " + arDate.getDate()+" "+ arDate.getFullYear();
    
  const cancelTicket=()=>{
    const url=`${URL}/user/booking/cancelTicket/${bookingId}`
    axios.put(url).then((response)=>{
      const result=response.data
                console.log(result.data.bookingId)

                if (result['status'] == 'success') {
                    //sessionStorage['flightId']=flightid
                    toast.success("Your ticket has been cancelled successfully")
                    navigate('/')
                  
                } else {
                    toast.error(result['error'])
                 
                }
    })
  }
  let pdfName=result.first_name + '_' + result.booking_id +".pdf"
  console.log(pdfName)
  const generatePDF =() =>{
    var doc = new jsPDF("p","pt","a3")
    doc.html(document.querySelector("#ticket"),{
        callback: function(pdf){
            pdf.save(pdfName)
        }
    })
}
    return(
        <div >
            <HeaderSelector/>
            <div class="col mt-4 d-flex justify-content-center">
            <h3 className="title">Ticket Details</h3>
            </div>
{/* <div > */}
  <div className="box" id='ticket' >
    
    <div className="ticket">
      <span className="airline">{result.airline_name}</span>
      <span className="airline airlineslip">{result.airline_name}</span>
      <span className="boarding">Ticket Details</span>
      <div className="content">
        <span className="jfk">{result.departure_airport_name}</span>
        <span className="scity">{result.source_city}</span>
        <span className="plane" style={{position:'absolute',top:'15px'}}>
          
          <img src={require("../Components/images/flightup.png")} class="rounded" width="55"/>
        </span>
        <span className="sfo">{result.arrival_airport_name}</span>
        <span className="dcity">{result.destination_city}</span>
        <span className="jfk jfkslip">{result.departure_airport_name}</span>
        <span className="plane planeslip"  style={{position:'absolute',top:'25px'}}>
          <img src={require("../Components/images/flightup.png")} class="rounded" width="35px"/>

        </span>
        <span className="sfo sfoslip">{result.arrival_airport_name}</span>
        <div className="sub-content">
          <span className="watermark">BookMyFlight </span>
          <span className="name">
            Passenger Name
            <br />
            <span>{result.first_name}{' '}{result.last_name}</span>
          </span>
          <span className="flight">
            Flight Id
            <br />
            <span className="flightspan">{result.flight_id}</span>
          </span>
          <span className="gate">
            Seat No <br />
            <span>{result.seat_no}</span>
          </span>
          <span className="seat">
          Booking Id <br />
            <span>{result.booking_id}</span>
          </span>
          <span className="fare">
          Total Fare <br />
            <span> Rs. {result.total_fare}</span>
          </span>
          <span className="boardingtime">
            Departure Time
            <br />
            <span>{departureDate}{'  '}{result.departure_time}</span>
          </span>
          <span className="arrivaltime">
            Arrival Time
            <br />
            <span>{arrivalDate}{'  '}{result.arrival_time}</span>
          </span>
          <span className="flight flightslip">
            Flight Id
            <br />
            <span>{result.flight_id}</span>
          </span>
          <span className="seat seatslip">
            Seat No <br />
            <span>{result.seat_no}</span>
          </span>
          <span className="name nameslip">
          Passenger Name
            <br />
            <span>{result.first_name}{' '}{result.last_name}</span>
          </span>
          <br />
          <div style={{backgroundColor:'white'}}>
          
          <span className="instruction ">
          Important Instructions
            <br/>
            <span className="instructionslip">• Web Check-in : Web Check-in is now a mandatory step for your air travel. For a hassle-free Web Check-in on Goibibo, please <Link to="/">click Here</Link></span>
          <br/><span className="instructionslip">• Check-in Time : Passenger to report 2 hours before departure. Check-in procedure and baggage drop will close 1 hour before departure.</span>
          <br/><span className="instructionslip">• Valid ID proof needed : Please carry a valid Passport and Visa (mandatory for international travel). Passport should have at least 6 months of validity at the time of travel.</span>
          </span>
          </div>
        </div>
      </div>
      <br/>
      <hr/>
      <div className="barcode" />
      <div className="barcode1">Status : {result.ticket_status}</div>
      <div className="barcode slip" />
    </div>
  </div>
  <div className="row" style={{marginTop:'475px'}}>
  <div class="col mt-4 d-flex justify-content-start" style={{marginLeft:'390px'}}>
    <button class="btn btn-success custom-button px-4 " width="40px" onClick={generatePDF}>Download Ticket</button>
  </div>
  <div class="col mt-4 d-flex justify-content-end" style={{marginRight:'390px'}}>
    <button class="btn btn-danger custom-button px-4 " onClick={cancelTicket}>Cancel Ticket</button>
  </div>
  </div>
  {/* </div> */}
  <div style={{position: "absolute",top:'690px'}}>
  <Footer/>
  </div>
        </div>
    )
}
export default TicketDetails