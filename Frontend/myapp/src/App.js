import "./App.css";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import Home from "./Pages/Home";
import Content2 from "./Components/Content2";
import Login from "./Pages/Login";
// s
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import FlightDetails from "./Pages/FlightDetails";
import BookingDetails from "./Pages/BookingDetails";
import Signup from "./Pages/SignUp";
import Offer from "./Pages/Offers";
import ProfileUpdate from "./Pages/ProfileUpdate";
import SelectPackage from "./Pages/SelectPackage";
import PaymentDetails from "./Pages/PaymentDetails";
import Payment from "./Pages/Payment";
import ResetPassword from "./Pages/ResetPass";
import ForgotPassword from "./Pages/ForgotPassword";
import VerifyOtp from "./Pages/OtpVerification";
import TicketDetails from "./Pages/TicketDetails";
import EditOffer from "./Pages/Admin/EditOffer";
import DeleteOffer from "./Pages/Admin/DeleteOffer";
import AddOffer from "./Pages/Admin/AddOffer";
import AdminHome from "./Pages/Admin/AdminHomePage";
import AddFlight from "./Pages/Admin/AddFlight";
import AddPackage from "./Pages/Admin/AddPackage";
import EditPackage from "./Pages/Admin/EditPackage";
import DeletePackage from "./Pages/Admin/DeletePackage";
import FlightStatus from "./Pages/FlightStatus";
import EditFlight from "./Pages/Admin/EditFlight";
import DeleteFlight from "./Pages/Admin/DeleteFlight";
import AddSchedule from "./Pages/Admin/AddSchedule";
import Schedule from "./Pages/Admin/ViewSchedule";
import EditSchedule from "./Pages/Admin/EditSchedule";
import AddCard from "./Pages/AddCard";
import TravelInfo from "./Pages/TravelInfo";
import CovidInfo from "./Pages/CovidInfo";
import FAQs from "./Pages/FAQs";
import MyBookings from "./Pages/MyBookings";
import CancelBooking from "./Pages/cancelBooking";
import Bookings from "./Pages/Admin/Bookings";
import Pdf from "./Pages/User/pdf";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/home" element={<Home />} />
          {/* <Route path="/home" element={<LoginHome />} /> */}
          <Route path="/user/flightDetails" element={<FlightDetails />} />
          <Route path="/user/bookingDetails" element={<BookingDetails />} />
          <Route path="/signUp" element={<Signup />} />
          <Route path="/profileUpdate" element={<ProfileUpdate />}></Route>
          <Route path="/offers" element={<Offer />}></Route>
          <Route path="/user/selectPackage" element={<SelectPackage />}></Route>
          <Route path="/user/paymentDetails" element={<PaymentDetails />}></Route>
          <Route path="/user/payment" element={<Payment />}></Route>
          <Route path="/resetpass" element={<ResetPassword />}></Route>
          <Route path="/forgotpassword" element={<ForgotPassword />}></Route>
          <Route path="/otpverify" element={<VerifyOtp />}></Route>
          <Route path="/resetpass" element={<VerifyOtp />}></Route>
          <Route path="user/ticket" element={<TicketDetails />}></Route>
          <Route path="/flightstatus" element={<FlightStatus />}></Route>
          <Route path="addCard" element={<AddCard />}></Route>
          <Route path="/travelInfo" element={<TravelInfo />}></Route>
          <Route path="/covidInfo" element={<CovidInfo />}></Route>
          <Route path="/FAQs" element={<FAQs />}></Route>
          <Route path="/mybooking" element={<MyBookings />}></Route>
          <Route path="/CancelBooking" element={<CancelBooking />}></Route>
          <Route path="/pdf" element={<Pdf/>}></Route>



          <Route path="/adminhome" element={<AdminHome />} />
          <Route path="/deleteoffer" element={<DeleteOffer />}></Route>
          <Route path="/addoffer" element={<AddOffer />}></Route>
          <Route path="/addflight" element={<AddFlight />}></Route>
          <Route path="/addpackage" element={<AddPackage />}></Route>
          <Route path="/editpackage" element={<EditPackage />}></Route>
          <Route path="/deletepackage" element={<DeletePackage />}></Route>
          <Route path="/editOffer" element={<EditOffer />}></Route>
          <Route path="/editflight" element={<EditFlight />}></Route>
          <Route path="/deleteflight" element={<DeleteFlight />}></Route>
          <Route path="/addschedule" element={<AddSchedule />}></Route>
          <Route path="/viewschedule" element={<Schedule />}></Route>
          <Route path="/editschedule" element={<EditSchedule />}></Route>
          <Route path="/bookings" element={<Bookings />}></Route>
        </Routes>
      </BrowserRouter>
      <ToastContainer theme="colored" />
    </div>
  );
}

export default App;
