#1 User Table

create table user (
user_id INTEGER primary key auto_increment,
first_name VARCHAR(40) NOT NULL,
last_name VARCHAR(40) NOT NULL,
email VARCHAR(40) UNIQUE NOT NULL,
password VARCHAR(600) NOT NULL,
role VARCHAR(20) DEFAULT "user"
);


insert into user (first_name, last_name, email, password) 
VALUES ("Ankit", "Dhawale", "ankit@gmail.com", "1234");

#ALTER TABLE table_name ADD CONSTRAINT constraint_name UNIQUE (column1, column2, ... column_n);
#ALTER TABLE user ALTER role SET DEFAULT "user";
#ALTER TABLE user MODIFY password varchar(20) NOT NULL;
#ALTER TABLE old_table_name RENAME TO new_table_name;


#2 Passenger table

create table passenger (
passenger_id INTEGER primary key auto_increment,
title VARCHAR(20) ,
first_name VARCHAR(40) ,
last_name VARCHAR(40) ,
mobile_no BIGINT ,
email VARCHAR(40) UNIQUE , 
country VARCHAR(40) ,
state VARCHAR(40) ,
city VARCHAR(40) ,
address VARCHAR(100),
date_of_birth DATE ,
covid_vaccinated BOOLEAN 
);


#3 Flight table

create table flight (
flight_id INTEGER primary key auto_increment,
airline_name VARCHAR(40),
departure_airport_name VARCHAR(40),
arrival_airport_name VARCHAR(40),
source_city VARCHAR(40),
destination_city VARCHAR(40),
business_class_fare DOUBLE ,
economy_class_fare DOUBLE,
business_class_capacity INTEGER,
economy_class_capacity INTEGER,
flight_total_capacity INTEGER
);


#4 Offer table

create table offer (
offer_id INTEGER primary key auto_increment,
promocode VARCHAR(40),
discount DOUBLE,
min_txn_amount DOUBLE,
applicable_on VARCHAR(200) DEFAULT "Offer valid for all Banks' Debit and Credit cards"
);


#5 Package table

create table package (
package_id INTEGER primary key auto_increment,
package_name VARCHAR(40),
seat_type VARCHAR(40),
food VARCHAR(40),
beverages VARCHAR(40),
baggage VARCHAR(40),
package_fare DOUBLE
);


#6 Schedule table

create table schedule(
schedule_id INTEGER primary key auto_increment,
flight_id int,
departure_date DATE,
departure_time TIME,
arrival_date DATE,
arrival_time TIME,
flight_status VARCHAR(40)
);


#7 Booking table

create table booking (
booking_id INTEGER primary key auto_increment,
passenger_id INTEGER,
schedule_id INTEGER,
user_id INTEGER,
package_id INTEGER,
offer_id INTEGER,
booking_date DATE,
passenger_count INTEGER,
fare_per_passenger DOUBLE,
seat_no INTEGER,
total_fare DOUBLE,
ticket_status VARCHAR(40)
);


#8 Payment card table

create table payment_card_detail (
card_id INTEGER primary key auto_increment,
user_id INTEGER,
name_on_card VARCHAR(40),
card_no BIGINT,
valid_till DATE,
cvv INTEGER
);

